-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-09-2022 a las 13:29:10
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vuelafacil`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avion`
--

CREATE TABLE `avion` (
  `idAvion` int(10) NOT NULL,
  `max_pasajeros` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `avion`
--

INSERT INTO `avion` (`idAvion`, `max_pasajeros`) VALUES
(963, '100'),
(987, '120');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutas`
--

CREATE TABLE `rutas` (
  `idRutas` int(10) NOT NULL,
  `origen` varchar(45) NOT NULL,
  `destino` varchar(45) NOT NULL,
  `distancia` varchar(45) NOT NULL,
  `duracion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rutas`
--

INSERT INTO `rutas` (`idRutas`, `origen`, `destino`, `distancia`, `duracion`) VALUES
(4578, 'Bogotá', 'Pasto', '1100', '70'),
(5689, 'Bogotá', 'Cartagena', '1020', '60');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiquete`
--

CREATE TABLE `tiquete` (
  `idTiquete` int(15) NOT NULL,
  `cantidad_pasajeros` int(100) NOT NULL,
  `precio_embalaje` varchar(45) NOT NULL,
  `precio_total` varchar(45) NOT NULL,
  `metodo_pago` varchar(45) NOT NULL,
  `usuario_cedula` int(12) NOT NULL,
  `idVuelo` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiquete`
--

INSERT INTO `tiquete` (`idTiquete`, `cantidad_pasajeros`, `precio_embalaje`, `precio_total`, `metodo_pago`, `usuario_cedula`, `idVuelo`) VALUES
(458712, 120, '200000', '2200000', 'Tarjeta de Crédito', 1020456987, 9852),
(563698, 100, '0', '1500000', 'Tarjeta Debito', 1020369852, 8524);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `cedula` int(12) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `contraseña` varchar(18) NOT NULL,
  `tipo_usuario` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`cedula`, `nombre`, `email`, `telefono`, `contraseña`, `tipo_usuario`) VALUES
(1020369852, 'Alberto Gomez', 'alberto.g@gamil.com', '3102599886', '987', 0),
(1020456987, 'Claudia Ramirez', 'c.ramirez@hotmail.com', '3102589745', '123', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `idVuelo` int(15) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_final` datetime NOT NULL,
  `idRutas` int(10) NOT NULL,
  `idAvion` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vuelo`
--

INSERT INTO `vuelo` (`idVuelo`, `descripcion`, `fecha_inicio`, `fecha_final`, `idRutas`, `idAvion`) VALUES
(8524, 'a', '2022-09-05 18:23:56', '2022-09-13 18:23:56', 4578, 963),
(9852, 'b', '2022-09-06 18:23:56', '2022-09-17 18:23:56', 5689, 987);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `avion`
--
ALTER TABLE `avion`
  ADD PRIMARY KEY (`idAvion`);

--
-- Indices de la tabla `rutas`
--
ALTER TABLE `rutas`
  ADD PRIMARY KEY (`idRutas`);

--
-- Indices de la tabla `tiquete`
--
ALTER TABLE `tiquete`
  ADD PRIMARY KEY (`idTiquete`),
  ADD KEY `fk vuelo` (`idVuelo`),
  ADD KEY `fk usuraio` (`usuario_cedula`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`idVuelo`),
  ADD KEY `fk rutas` (`idRutas`),
  ADD KEY `fk avion` (`idAvion`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tiquete`
--
ALTER TABLE `tiquete`
  ADD CONSTRAINT `fk usuraio` FOREIGN KEY (`usuario_cedula`) REFERENCES `usuario` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk vuelo` FOREIGN KEY (`idVuelo`) REFERENCES `vuelo` (`idVuelo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD CONSTRAINT `fk avion` FOREIGN KEY (`idAvion`) REFERENCES `avion` (`idAvion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk rutas` FOREIGN KEY (`idRutas`) REFERENCES `rutas` (`idRutas`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
